package PlaceOfEmployment;
							//inherits from the Employee class
public class ProductionWorker extends Employee {
	//instance fields
	private int shift;
	private double payRate;
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	//constructor followed by an overloaded no-arg constructor
	public ProductionWorker(String n, String num, String date, int sh, double rate){
		setName(n);
		setEmployeeNumber(num);
		setHireDate(date);
		//The previous 3 statements call methods that belong to the Employee class
		setShifts(sh);
		setPayRate(rate);
	}
	public ProductionWorker(){
		
	}
	public void setShifts(int s){
		shift = s;
	}
	public void setPayRate(double p){
		payRate = p;
	}
	public int getShift(){
		return shift;
	}
	public double getPayRate(){
		return payRate;
	}
	//Overridden toString method
	public String toString(){
		String str = "Employee Name: " + getName() +"\nEmployee number: " + getEmployeeNumber() + 
				"\nHire date: " + getHireDate() + "\nShifts: " + getShift() + "\nPay rate: $" + getPayRate();
		
		return str;
	}
}
