package PlaceOfEmployment;

public class Launcher {


	public static void main(String[] args) {
		//New employee object that's instantiated using the TeamLeader class constructor
		Employee emp1 = new TeamLeader("Bob","123-A","12/21/12",1,12,2000,62,64);
		//prints the employee object's toString method, showcases polymorphism via the output results in the TeamLeader class's toString method
		System.out.println(emp1.toString());

	}
}
