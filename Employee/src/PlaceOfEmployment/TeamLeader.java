package PlaceOfEmployment;

public class TeamLeader extends ProductionWorker{
	//instance fields
	private double monthlyBonus;
	private double requiredTrainingHours;
	private double trainingHoursAttended;
	//constructor
	public TeamLeader(String n, String num, String date,int sh, double rate, double mb, double rth, double tha){
		setName(n);
		setEmployeeNumber(num);
		setHireDate(date);
		setShifts(sh);
		setPayRate(rate);
		//The following 3 statements are the only methods that specifically belong to the TeamLeader class, 
		//The previous statements in the constructor call methods that belong to the ProductionWorker and Employee classes
		setMonthlyBonus(mb);
		setRequiredTrainingHours(rth);
		setTrainingHoursAttended(tha);
	}
	//no-arg constructor
	public TeamLeader(){
		
	}
	//appropriate accessors and mutators
	public double getMonthlyBonus() {
		return monthlyBonus;
	}
	public void setMonthlyBonus(double monthlyBonus) {
		this.monthlyBonus = monthlyBonus;
	}
	public double getRequiredTrainingHours() {
		return requiredTrainingHours;
	}
	public void setRequiredTrainingHours(double requiredTrainingHours) {
		this.requiredTrainingHours = requiredTrainingHours;
	}
	public double getTrainingHoursAttended() {
		return trainingHoursAttended;
	}
	public void setTrainingHoursAttended(double trainingHoursAttended) {
		this.trainingHoursAttended = trainingHoursAttended;
	}
	//Overridden toString method
	public String toString(){
		String str = "Employee Name: " + getName() +"\nEmployee number: " + getEmployeeNumber() + 
				"\nHire date: " + getHireDate() + "\nShifts: " + getShift() + "\nPay rate: $" + getPayRate() +
				"\nMonthly bonus: $" + getMonthlyBonus() + "\nRequired Training hours: " + getRequiredTrainingHours() +
				" |Training hours attended: " + getTrainingHoursAttended();
		
		return str;
	}
	

}

