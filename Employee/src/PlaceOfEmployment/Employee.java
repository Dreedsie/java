package PlaceOfEmployment;

public class Employee {
	//Instance fields 
	private String name;
	private String employeeNumber;
	private String hireDate;
	//constructor, followed by an overloaded no-arg constructor
	public Employee(String n, String num, String date){
		setName(n);
		if(isValidEmpNum(num))
		setEmployeeNumber(num);
		else setEmployeeNumber("Invalid employee number");
		
		setHireDate(date);
	}
	public Employee(){
		
	}
	//accessors and mutators
	public void setName(String n){
		name = n;
	}
	public void setEmployeeNumber(String e){
		employeeNumber = e;
	}
	public void setHireDate(String h){
		hireDate = h;
	}
	public String getName(){
		return name;
	}
	public String getEmployeeNumber(){
		return employeeNumber;
	}
	public String getHireDate(){
		return hireDate;
	}
	//method that checks the validity of the employee number
	private boolean isValidEmpNum(String e){
		boolean yes = false;
		char[] empNum = e.toCharArray();
		if(empNum.length  ==5 && Character.isDigit(empNum[0]) && Character.isDigit(empNum[1]) && Character.isDigit(empNum[2])
				&& empNum[3] == '-' && Character.toString(empNum[4]).matches("^[z-nZ-N0-9]*$"))
			yes = true;
		
		return yes;
	}
	//toString method
	public String toString(){
		String str = "Employee name: " + getName() + "\nEmployee Number: " + getEmployeeNumber() + "\nHire date: " + getHireDate();
		return str;
	}
}


