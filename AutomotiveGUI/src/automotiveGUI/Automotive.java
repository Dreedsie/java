package automotiveGUI;
import javafx.application.Application;
import javafx.stage.Stage;

import java.text.DecimalFormat;

import javafx.geometry.Insets;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class Automotive extends Application {
	private double total;
	
	public void addToTotal(double amount){
		total += amount;
	}
	public void addNonRoutine(double a, int h){
		total += 20 * h;
		total += a;
	}
	public void clearTotal(){
		total = 0;
	}
	public double getTotal() {
		return total;
	}
	public Automotive(){
		
	}
	public static void main(String[] args) {
		launch();

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Label l1 = new Label("Oil Change ($26.00)");
		Label l2 = new Label("Lube Job ($18.00)");
		Label l3 = new Label("Radiator Flush ($30.00)");
		Label l4 = new Label("Transmission Flush ($80.00)");
		Label l5 = new Label("Inspection ($15.00)");
		Label l6 = new Label("Muffler Replacement ($100.00)");
		Label l7 = new Label("Tire Rotation ($20.00)");
		Label l8 = new Label("Routine Services");
		Label l9 = new Label("Nonroutine Services");
		Label l10 = new Label("Parts Charges: ");
		Label l11 = new Label("Hours of Labor: ");
		Label l12 = new Label("Total Charges: ");
		
		TextField tf1 = new TextField();
		TextField tf2 = new TextField();
		
		Button b1 = new Button("Calculate Charges");
		Button b2 = new Button("Exit");
		
		CheckBox cb1 = new CheckBox();
		CheckBox cb2 = new CheckBox();
		CheckBox cb3 = new CheckBox();
		CheckBox cb4 = new CheckBox();
		CheckBox cb5 = new CheckBox();
		CheckBox cb6 = new CheckBox();
		CheckBox cb7 = new CheckBox();
		
		GridPane gp = new GridPane();
		gp.add(cb1, 0, 1);
		gp.add(cb2, 0, 2);
		gp.add(cb3, 0, 3);
		gp.add(cb4, 0, 4);
		gp.add(cb5, 0, 5);
		gp.add(cb6, 0, 6);
		gp.add(cb7, 0, 7);
		gp.add(l1, 1, 1);
		gp.add(l2, 1, 2);
		gp.add(l3, 1, 3);
		gp.add(l4, 1, 4);
		gp.add(l5, 1, 5);
		gp.add(l6, 1, 6);
		gp.add(l7, 1, 7);
		gp.setHgap(10);
		gp.setVgap(10);
		gp.setPadding(new Insets(10));
		
		HBox hbox1 = new HBox(l8);
		hbox1.setPadding(new Insets(10));
		
		HBox hbox2 = new HBox(l9);
		
		GridPane gp2 = new GridPane();
		gp2.add(hbox2, 0, 0);
		gp2.add(l10, 0, 1);
		gp2.add(l11, 0, 2);
		gp2.add(tf1, 1, 1);
		gp2.add(tf2, 1, 2);
		gp2.add(b2, 1, 4);
		gp2.setHgap(10);
		
		HBox hbox3 = new HBox(10,b1);
		HBox hbox4 = new HBox(l12);
		gp2.add(hbox3, 0, 4);
		gp2.add(hbox4, 0, 6);
		gp2.setPadding(new Insets(15));
		
		BorderPane bp = new BorderPane();
		bp.setTop(hbox1);
		bp.setCenter(gp);
		bp.setBottom(gp2);
		
		Automotive a = new Automotive();
		DecimalFormat df = new DecimalFormat();
		
		b1.setOnAction(e -> {
			a.clearTotal();
			if(cb1.isSelected())
				a.addToTotal(26);
			if(cb2.isSelected())
				a.addToTotal(18);
			if(cb3.isSelected())
				a.addToTotal(30);
			if(cb4.isSelected())
				a.addToTotal(80);
			if(cb5.isSelected())
				a.addToTotal(15);
			if(cb6.isSelected())
				a.addToTotal(100);
			if(cb7.isSelected())
				a.addToTotal(20);
			if(!tf1.getText().trim().isEmpty() || !tf2.getText().trim().isEmpty())
			try{
				a.addNonRoutine(Double.parseDouble(tf1.getText()), Integer.parseInt(tf2.getText()));
			}
			catch(Exception e2){
				
			}
			finally{
				l12.setText("Total Charges: $" + df.format(a.getTotal()));
			}
			l12.setText("Total Charges: $" + df.format(a.getTotal()));
		});
		
		
		Scene scene = new Scene(bp,500,450);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Ranken's Automotive Maintenance");
		primaryStage.show();
		b2.setOnAction(e -> {
			primaryStage.close();
		});
		
		
		
		
	}
	
	

}
